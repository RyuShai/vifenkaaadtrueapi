:orphan:

.. _keyword:

Giải thích các khái niệm và định nghĩa keyword

.. _human_track_id:

Human track id
==============

|   Giá trị unique đại diện cho 1 người mà thiết bị detect được.

|   Nếu 1 người (gọi là objectA) xuất hiện trong vùng khả kiến của camera và dữ liệu là liên tục không đứt đoạn thì họ sẽ được gán 1 uniqueID.

.. note::
    Nếu objectA đi ra khỏi vùng khả kiến của camera và quay lại, object sẽ được gán 1 giá trị unique khác.

    Nếu objectA không đi ra khỏi vùng khả kiến của camera, nhưng lại bị che khuất bởi 1 object khác đủ lâu thì objectA cũng sẽ được gán 1 uniqueID khác.

.. _faceTrackId:

Face track id
==============

|   Giá trị unique đại diện cho 1 khuôn mặt mà thiết bị detect ra được. Giá trị này độc lập với :ref:`human_track_id`.
|   Nếu khuôn mặt bị che khuất hoặc quay đi đủ lâu thì sẽ được gán 1 giá trị track id khác.

.. _face_head_pose:

Face head pose
===============

|   Tiến trình xử lí ảnh cho phép biết được 3 giá trị của góc mặt: pitch, yaw, roll từ đó xác định hướng nhìn của khuôn mặt.

.. note:: Do bản thân viewer nhìn vào màn hình chứ không nhìn vào camera nên tùy vào loại camera, vị trí và góc chiếu mà bộ 3 giá trị pitch, yaw,, roll sẽ thay đổi

