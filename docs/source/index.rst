.. VifenkaaAdtrueAPI documentation master file, created by
   sphinx-quickstart on Tue Jan 25 10:11:15 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to VifenkaaAdtrueAPI's documentation!
---------------------------------------------

.. toctree::
   :maxdepth: 2
   :caption: Requirements

   requirements

.. toctree::
   :maxdepth: 2
   :caption: Intergrate

   api
   register
   manual

.. toctree::
   :maxdepth: 2
   :caption: Formula

   kpi_formula

.. Indices and tables
.. ==================

.. * :ref:`requiments`

.. * :ref:`api`

.. * :ref:`formula`





