.. toctree::
    :maxdepth: 1
    :caption: contents

.. _requiments:

**Requirements**
-----------------

*Require environment to develop and deploy* 

**Environment**
======================================

| Host os : window 10 / window  11
| IDE: Android studio artic fox | 2020.3.1 | path 4

**Deploy device**
======================
| Anroid device 
| Android box x96-x4 + logitech c615
| MinSdk 24

**Download model**
============================

| Model binary file được build bên ngoài, fine-tune cho từng platform để tối ưu hiệu năng, nó có thể được cập nhật độc lập với API.
| Đã update chức năng download model, việc download model sẽ được quản lí bởi vifenkaa libs