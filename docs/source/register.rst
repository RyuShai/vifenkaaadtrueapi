.. toctree::
   :maxdepth: 1
   :caption: acount & devices

**Tài khoản & thiết bị**
-------------------------

**Đăng kí tài khoản**
=========================

| Tài khoản khách hàng sử dụng đẻ đăng nhập hệ thống cloud, kiểm soát AI engine và thiết bị.
| Đăng kí tài khoản tại hệ thống của `Vifenkaa cloud <vifen.ecoai.vn>`_ 

**Lấy register key**
======================

    Login vào hệ thống của `Vifenkaa cloud <vifen.ecoai.vn>`_ 


    .. image:: Images/VifenkaaCloud/photo_2022-03-07_13-48-27.jpg
        :alt: login page


Click vào button "Mã đăng kí" ở góc trên, bên phải màn hình để lấy register key.


.. image:: Images/VifenkaaCloud/photo_2022-03-07_13-47-24.jpg

.. note:: Lưu trữ key ở nơi an toàn và khi sử dụng lên gửi key đến thiết bị từ hệ thống của bạn thay vì lưu trữ trên thiết bị đầu cuối.

**Đăng kí thiết bị mới**
===========================

    Khi thư viện Vifenkaa được kích hoạt và được cung cấp register key, việc đăng kí sẽ tự động diễn ra. Sau khi thiết bị đã đăng kí người dùng cần truy cập vào `Vifenkaa cloud <vifen.ecoai.vn>`_  để kích hoạt thiết bị.

    .. image:: Images/VifenkaaCloud/photo_2022-03-07_13-47-24.jpg
        :alt: manage device page


| Trạng thái của thiết bị có thể được tìm thấy ở cột trạng thái

| Định danh của thiết bị có thể thấy ở cột định danh thiết bị

**Kiểm soát trạng thái thiết bị**
=====================================

| **Chờ duyệt:** Thiết bị đã kết nối thành công lên cloud. Cần user cấp phép để gửi dữ liệu lên cloud để phân tích.
| **Đã kích hoạt:** Thiết bị đã kết nối đến cloud và đã có quyền gửi dữ liệu lên cloud.

.. * **Xóa thiết bị**
.. ===================

..     *Cần bổ sung màn hình xóa thiết bị và sơ đồ mô tả*

.. * **Xóa tài khoản**
.. ====================

..     *Cần bổ sung màn hình xóa tài khoản và sơ đồ mô tả*