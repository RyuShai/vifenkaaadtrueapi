.. toctree::
    :maxdepth: 1
    :caption: manual   
    
      
**Mẫu báo cáo** 
================


Nhấn vào biểu tưởng chart ở cạnh trái màn hình để xem các mẫu báo cáo có sẵn.

.. image:: Images/VifenkaaCloud/photo_2022-03-07_13-48-33.jpg
    :alt: report page

.. Hướng dẫn sử dụng hệ thống vifenkaa
.. ------------------------------------

.. * **Kích hoạt engine**

.. * **Chuyển chế độ (mode EDGE | CLOUD)**

.. * **Đăng kí nhận diện khuôn mặt (face recognition)**

.. * **Thêm ca

