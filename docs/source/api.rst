.. toctree::
    :maxdepth: 1
    :caption: Formula

Tài liệu mô tả API liên quan đến java android interface

.. _api:

**Module structure**
---------------------
.. image:: Images/ModuleStructure.png
     :alt: module VifenkaaAi

* VifenkaaAi : `import module`
    * cpp : `jni interface, do not modify`
    * java : `java interface, using to implement`
    * jniLibs : `c++ libs, apk should contain all of them`

**Java Interface**
------------------

Class VifenkaaAdtrueBuilder 
===========================
    .. code-block:: java

        public class VifenkaaAdtrueBuilder implements VifenkaaAiBuilder
       
Method

.. code-block:: java

        public VifenkaaAiBuilder camDevice(CameraDevice camera){}

* `CameraDevice object <https://developer.android.com/reference/android/hardware/camera2/CameraDevice>`_ provide input image buffer to VifenkaaAi process. 

#. camera: CameraDevice
    `Can not be null`


.. code-block:: java

    public VifenkaaAiBuilder printImages(boolean printMode)

* Set print debug image: ON/OF

#. printMode:  if true image debug sẽ in ra ở thư mục /storage/emulated/0/model. Default is false

.. note::

    Images sẽ được lưu  tại địa chỉ: `/storage/emulated/0/model/[timestamp].jpg`


.. code-block:: java

    public VifenkaaAiBuilder registerKey(String value)

#. value: User's register key. Key dùng để đăng kí thiết bị mới. Cung cấp thông qua tài khoản khách hàng trên `Vifen cloud <vifen.ecoai.vn>`_ . Key cũng được sử dụng để xác thực gói tin khi gửi data từ client đến cloud để phân tích.
    `Can not be null`

.. code-block:: java

    public VifenkaaAiBuilder registerId(String id)

#. id: Hệ thống Vifenkaa DOOH hướng đến quản lí các thiết bị IoT với số lượng hàng nghìn thiết bị vì thế để khách hàng dễ quản lí, hệ thống cung cấp registerId để khách hàng có thể dễ dàng định danh thiết bị như 1 unique id. Format sẽ do khách hàng tự do quy định. Default is empty


.. code-block:: java

        public VifenkaaProcess build() 

* Build VifenkaaAi object then return

Class VifenkaaProcess 
=========================

.. code-block::

    public class VifenkaaProcess extends Application

Method
==========================

.. code-block:: java

        public void openCamera()

* initial AI process then force to work loop:
    * setup ImageReader
    * start cameraDevice object 
    * read image bufer 
    * call jni to detect
    * send result to backend server

.. code-block:: java

        private void createClient()

* Private method using to send result to backend server




