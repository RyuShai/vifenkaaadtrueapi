.. toctree::
    :maxdepth: 1
    :caption: contents

.. _formula:

Formula
--------


    Giải thích về công thức để tính ra KPI

**Android app result:**

    Kết quả được xử lý trực tiếp tại android device thông qua dữ liệu hình ảnh trước khi gửi về cloud server gồm 2 thông tin cơ bản:

    #. Số lượng và giá trị của :ref:`human_track_id`

    #. số lượng và giá trị của :ref:`faceTrackId`

KPI
===

1. **Number people passed:**


|   Số lượng người đi qua màn hình, dược đếm tại thời điển người đó biến mất khỏi vùng khả kiến của camera.
|   Kết quả của kpi này chính là số lượng :ref:`human_track_id` unique trong 1 khoảng thời gian.

2. **number of view:**

|   Số lượng người xem quảng cáo. Được đếm tại thời điểm họ bắt đầu nhìn vào màn hình quảng cáo.
|   Từ kết quả detect, dữ liệu ảnh được gửi lên cloud để tiếp tục xử lý :ref:`face head pose <face_head_pose>` để đánh giá khuôn mặt đó có đang nhìn vào màn hình hay không.
|   Đếm số lượng kết quả phù hợp sau face head pose process sẽ cho kết quả của kpi này.

3. **duration per view**

|   Khoảng thời gian nhìn vào màn hình của mỗi lượt view.
|   Được xác định bằng cách đo thời điểm đâu tiên nhìn vào màn hình và thời điểm cuối cùng nhìn vào màn hình.
|   `Kết quả của duration sẽ được làm tròn đến giây kế tiếp`

